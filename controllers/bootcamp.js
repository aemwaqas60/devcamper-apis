const Bootcamp = require("../models/Bootcamps");

// @desc Find all bootcamps
// @route GET /api/v1/bootcamp
// @access     public
exports.find = async (req, res, next) => {
  console.log("Find All Bootcamp Controller");

  try {
    const bootcamps = await Bootcamp.find();
    res.status(200).json({
      success: true,
      data: bootcamps
    });
  } catch (err) {
    res.status(400).json({
      success: false
    });
  }
};

// @desc Find single bootcamp
// @route  GET /api/v1/bootcamp/:id
// @access   public
exports.findById = async (req, res, next) => {
  const id = req.params.id;
  console.log(`Find By id Bootcamp controller ${id}`);

  try {
    const bootcamp = await Bootcamp.findById(id);
    res.status(200).json({
      success: true,
      data: bootcamp
    });
  } catch (err) {
    res.status(400).json({
      success: false
    });
  }
};

// @desc   Save new bootcamp
// @route  POST /api/v1/bootcamp/
// @access private
exports.save = async (req, res, next) => {
  console.log(`Create new Bootcamp controller ${req.body}`);
  try {
    const bootcamp = await Bootcamp.create(req.body);
    res.status(201).json({
      success: true,
      message: "Bootcamp successfully Created.",
      data: bootcamp
    });
  } catch (err) {
    res.status(400).json({
      success: false,
      error: err
    });
  }
};

// @desc  Update bootcamp
// @route UPDATE /api/v1/bootcamp/:id
// @access   private
exports.update = async (req, res, next) => {
  const id = req.params.id;
  const body = req.body;
  console.log(`Bootcamp Update controller ${id}`);

  try {
    const updatedBootcamp = await Bootcamp.findByIdAndUpdate(id, body, {
      new: true,
      runValidators: true
    });
    res.status(200).json({
      success: true,
      message: "Bootcamp has been successfully updated",
      data: updatedBootcamp
    });
  } catch (err) {
    res.status(400).json({
      success: false,
      error: err
    });
  }
};

// @desc Delete bootcamp
// @route DELETE /api/v1/bootcamp/:id
// @access  private
exports.remove = async (req, res, next) => {
  const id = req.params.id;
  console.log(`Bootcamp Remove Controller ${id}`);

  try {
    const deletedBootcamp = await Bootcamp.findByIdAndDelete(id);
    res.status(200).json({
      success: true,
      message: "Bootcamp has deleted successfully",
      data: deletedBootcamp
    });
  } catch (err) {
    res.status(400).json({
      success: flase
    });
  }
};
