const express = require("express");
const colors = require('colors');
const dotenv = require("dotenv");
dotenv.config({ path: "./config/config.env" });
const morgan = require("morgan");
const connectDB= require('./config/db');

const bootcampRoutes = require("./routes/bootcamp");

const app = express();

// body parser middleware
app.use(express.json());

// connect to data
connectDB();

//morgan logger middleware
app.use(morgan("dev"));

//routes
app.use("/api/v1/bootcamps", bootcampRoutes);

const PORT = process.env.PORT || 3000;
app.listen(PORT, () => {
  console.log(
    `Server is running in ${process.env.NODE_ENV} mode on port ${PORT}`.cyan.bold
  );
});
