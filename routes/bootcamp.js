const express = require("express");
const router = express.Router();
const {
  find,
  findById,
  save,
  update,
  remove
} = require("../controllers/bootcamp");
router
  .route("/")
  .get(find)
  .post(save);

router
  .route("/:id")
  .get(findById)
  .put(update)
  .delete(remove);

module.exports = router;
