const mongoose = require("mongoose");

const connectDB = () => {
  const uri = "mongodb://localhost:27017/devCamper";
  const options = {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: false,
    useCreateIndex: true
  };

  mongoose.connect(uri, options);

  mongoose.connection.on("connected", () => {
    console.log(
      `Mongoose default connection is open to ${mongoose.connection.host}`.cyan
        .bold
    );
  });

  mongoose.connection.on("error", err => {
    console.log(
      `Mongoose default connection has occured error: ${err}`.yellow.bold
    );
  });

  mongoose.connection.on("disonnected", () => {
    console.log(`Mongoose default connection is disconnected`.red.bold);
  });
};

module.exports = connectDB;
